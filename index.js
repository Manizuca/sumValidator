import twoNumberSum from "./src/index.js";

function test() {
  const array = [3, 5, -4, 8, 11, 1, -1, 6];
  const targetSum = 10;
  const result = twoNumberSum(array, targetSum);
}
test();

export default twoNumberSum;
