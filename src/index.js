export default function twoNumberSum(array, targetSum) {
  let result = [];

  while (array.length && !result.length) {
    const number1 = array.shift();
    const number2 = array.find(number2 =>
      (number1 + number2 == targetSum)
    );

    if (number2 != undefined) result.push(number1, number2);
  }

  console.log(result.length && `El array resultante es: [${result}]` || "No hay coincidencias")
  return result;
}
